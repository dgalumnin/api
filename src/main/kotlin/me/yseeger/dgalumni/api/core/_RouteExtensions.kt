package me.yseeger.dgalumni.api.core

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.sessions.*
import io.ktor.util.pipeline.*
import me.yseeger.dgalumni.api.data.Account
import org.jetbrains.exposed.sql.transactions.transaction

suspend fun PipelineContext<Unit, ApplicationCall>.requireAccount(): Account? {
    val account = call.sessions.get<DGAlumniSession>()?.let { transaction { Account.findById(it.accountId) } }
    if (account == null) {
        call.respond(HttpStatusCode.Unauthorized)
    }
    return account
}

suspend fun PipelineContext<Unit, ApplicationCall>.requireAdmin(): Account? {
    val account = call.sessions.get<DGAlumniSession>()?.let { transaction { Account.findById(it.accountId) } }
    if (account == null) {
        call.respond(HttpStatusCode.Unauthorized)
        return null
    }
    if (!account.admin) {
        call.respond(HttpStatusCode.Forbidden)
    }
    return account
}