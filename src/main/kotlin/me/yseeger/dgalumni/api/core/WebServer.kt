package me.yseeger.dgalumni.api.core

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.metrics.micrometer.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.sessions.*
import io.micrometer.core.instrument.binder.jvm.ClassLoaderMetrics
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics
import io.micrometer.core.instrument.binder.system.ProcessorMetrics
import kotlinx.serialization.SerializationException
import me.yseeger.dgalumni.api.config.Config
import me.yseeger.dgalumni.api.controller.auth
import me.yseeger.dgalumni.api.controller.years
import me.yseeger.dgalumni.api.metrics.Metrics

data class DGAlumniSession(val accountId: Long)

fun setupWebServer() = embeddedServer(Netty, Config.HTTP_PORT) {
    install(CORS)
    install(DefaultHeaders)
    install(MicrometerMetrics) {
        registry = Metrics
        meterBinders = listOf(
            ClassLoaderMetrics(),
            JvmMemoryMetrics(),
            JvmGcMetrics(),
            ProcessorMetrics(),
            JvmThreadMetrics(),
        )
    }

    install(StatusPages) {
        exception<SerializationException> {
            call.respond(HttpStatusCode.BadRequest)
        }
    }

    install(Sessions) {
        cookie<DGAlumniSession>("DGALUMNI_API_SESSION")
    }

    routing {
        get("/metrics/prometheus") {
            call.respond(Metrics.scrape())
        }

        auth()
        years()
    }
}.start()