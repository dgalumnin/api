package me.yseeger.dgalumni.api.data

import me.yseeger.dgalumni.api.util.Snowflakes
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable

object Users : IdTable<Long>() {
    override val id = long("id").clientDefault { Snowflakes.newId() }.entityId()
    val firstName = varchar("first_name", 20)
    val lastName = varchar("last_name", 20)
    override val primaryKey = PrimaryKey(id)
}

class User(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<User>(Users)

    var firstName by Users.firstName
    var lastName by Users.lastName
}