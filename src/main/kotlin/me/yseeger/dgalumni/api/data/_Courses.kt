package me.yseeger.dgalumni.api.data

import me.yseeger.dgalumni.api.util.Snowflakes
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable

object Courses : IdTable<Long>() {
    override val id = long("id").clientDefault { Snowflakes.newId() }.entityId()
    val name = integer("name")
    val year = reference("year", Years)
    override val primaryKey = PrimaryKey(id)
}

class Course(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Course>(Courses)

    var name by Courses.name
    var year by Year referencedOn Courses.year
}