package me.yseeger.dgalumni.api.data

import me.yseeger.dgalumni.api.util.Snowflakes
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable

object Accounts : IdTable<Long>() {
    override val id = long("id").clientDefault { Snowflakes.newId() }.entityId()
    val email = varchar("email", 255)
    val password = varchar("password", 512)
    val verified = bool("verified").default(false)
    val admin = bool("admin").default(false)
    val disabled = bool("disabled").default(false)
    val user = reference("user", Users).nullable()
    override val primaryKey = PrimaryKey(id)
}

class Account(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Account>(Accounts)

    var email by Accounts.email
    var password by Accounts.password
    var verified by Accounts.verified
    var admin by Accounts.admin
    var disabled by Accounts.disabled
    val user by User optionalReferencedOn Accounts.user
}