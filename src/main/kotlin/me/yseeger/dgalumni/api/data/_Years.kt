package me.yseeger.dgalumni.api.data

import me.yseeger.dgalumni.api.util.Snowflakes
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable

object Years : IdTable<Long>() {
    override val id = long("id").clientDefault { Snowflakes.newId() }.entityId()
    val year = integer("year")
    val isClass = bool("is_class")
    override val primaryKey = PrimaryKey(id)
}

class Year(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Year>(Years)

    var year by Years.year
    var isClass by Years.isClass
    val courses by Course referrersOn Courses.year
}