package me.yseeger.dgalumni.api.config

object Config : EnvConfig("API_") {
    val DEVELOPMENT: Boolean by getEnv(false) { it.toBoolean() }
    val SENTRY_DSN: String by getEnv("")

    val HTTP_PORT: Int by getEnv(3500) { it.toInt() }

    val DB_HOST: String by getEnv()
    val DB_PORT: String by getEnv()
    val DB_USER: String by getEnv()
    val DB_PASSWORD: String by getEnv()
    val DB_NAME: String by getEnv()
}