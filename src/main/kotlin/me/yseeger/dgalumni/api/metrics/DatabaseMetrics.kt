package me.yseeger.dgalumni.api.metrics

import com.zaxxer.hikari.HikariDataSource
import io.micrometer.core.instrument.Gauge

fun collectDatabaseMetrics(dataSource: HikariDataSource) {
    Gauge.builder("db_connections") {
        dataSource.hikariPoolMXBean.activeConnections
    }.tags("status", "active").register(Metrics)

    Gauge.builder("db_connections") {
        dataSource.hikariPoolMXBean.idleConnections
    }.tags("status", "idle").register(Metrics)

    Gauge.builder("db_connections") {
        dataSource.hikariPoolMXBean.totalConnections
    }.tags("status", "total").register(Metrics)
}