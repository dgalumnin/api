package me.yseeger.dgalumni.api.util

import xyz.downgoon.snowflake.Snowflake

object Snowflakes {
    private val snowflake = Snowflake(1, 1)

    fun newId() = snowflake.nextId()
}
