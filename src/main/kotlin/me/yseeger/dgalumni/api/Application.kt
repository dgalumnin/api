package me.yseeger.dgalumni.api

import at.favre.lib.crypto.bcrypt.BCrypt
import com.zaxxer.hikari.HikariDataSource
import io.sentry.Sentry
import me.yseeger.dgalumni.api.config.Config
import me.yseeger.dgalumni.api.core.setupWebServer
import me.yseeger.dgalumni.api.data.*
import me.yseeger.dgalumni.api.metrics.collectDatabaseMetrics
import mu.KotlinLogging
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

val log = KotlinLogging.logger { }

fun main() {
    val time = System.currentTimeMillis()
    log.info { "Starting..." }

    if (!Config.DEVELOPMENT)
        Sentry.init(Config.SENTRY_DSN)

    setupDatabase()
    setupWebServer()
    log.info { "Started." }
    log.info { "Startup took ${System.currentTimeMillis() - time}ms." }
}

fun setupDatabase() {
    val dataSource = HikariDataSource().apply {
        jdbcUrl = "jdbc:postgresql://${Config.DB_HOST}:${Config.DB_PORT}/${Config.DB_NAME}"
        username = Config.DB_USER
        password = Config.DB_PASSWORD
    }
    Database.connect(dataSource)
    collectDatabaseMetrics(dataSource)

    transaction {
        SchemaUtils.createMissingTablesAndColumns(Accounts, Users, Years, Courses)
    }

    // Create Seed Account
    val adminExists = transaction {
        return@transaction Account.find { Accounts.admin eq true }.any()
    }
    if (!adminExists) {
        transaction {
            Account.new {
                email = "admin@dgalumni.net"
                password = BCrypt.withDefaults().hashToString(12, "admin".toCharArray())
                disabled = false
                admin = true
            }
            log.info { "Created default admin account. Use 'admin@dgalumni.net' as email address and 'admin' as password for signing in." }
        }
    }
}