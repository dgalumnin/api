package me.yseeger.dgalumni.api.controller

import at.favre.lib.crypto.bcrypt.BCrypt
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import me.yseeger.dgalumni.api.core.DGAlumniSession
import me.yseeger.dgalumni.api.data.Account
import me.yseeger.dgalumni.api.data.Accounts
import me.yseeger.dgalumni.api.data.Course
import mu.KotlinLogging
import org.jetbrains.exposed.sql.transactions.transaction

private val log = KotlinLogging.logger {}

@Serializable
data class SignUpRequest(
    val email: String,
    val password: String,
    val firstName: String,
    val lastName: String,
    val course: Long
)

@Serializable
data class SignInRequest(
    val email: String,
    val password: String
)

fun Route.auth() {
    route("/auth") {
        post("signUp") {
            val req = Json.decodeFromString<SignUpRequest>(call.receiveText())
            // Length Validation
            if (req.email.length > 256) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("signUp/email-too-long")))
                return@post
            }
            if (req.password.length > 512) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("signUp/password-too-long")))
                return@post
            }
            if (req.firstName.length > 20) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("signUp/first-name-too-long")))
                return@post
            }
            if (req.lastName.length > 20) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("signUp/last-name-too-long")))
                return@post
            }
            if (req.course.toString().length > 20) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("signUp/invalid-course")))
                return@post
            }

            // Database Checks
            val emailExists = transaction {
                return@transaction Account.find { Accounts.email eq req.email }.any()
            }
            if (emailExists) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("signUp/email-exists")))
                return@post
            }
            val course = transaction {
                return@transaction Course.findById(req.course)
            }
            if (course == null) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("signUp/invalid-course")))
                return@post
            }
            //TODO: We need to implement courses before
            call.respond(HttpStatusCode.Created)
        }

        post("/signIn") {
            val req = Json.decodeFromString<SignInRequest>(call.receiveText())
            if (req.email.length > 256) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("signIn/invalid-email")))
                return@post
            }
            if (req.password.length > 512) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("signIn/invalid-password")))
                return@post
            }

            val account = transaction {
                return@transaction Account.find { Accounts.email eq req.email }.firstOrNull()
            }
            if (account == null) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("signIn/email-not-found")))
                return@post
            }

            if (!BCrypt.verifyer().verify(req.password.toCharArray(), account.password.toCharArray()).verified) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("signIn/wrong-password")))
                return@post
            }

            call.sessions.set(DGAlumniSession(account.id.value))
            call.respond(HttpStatusCode.OK)
        }

        post("/logout") {
            call.sessions.clear<DGAlumniSession>()
            call.respond(HttpStatusCode.OK)
        }
    }
}