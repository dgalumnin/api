package me.yseeger.dgalumni.api.controller

import kotlinx.serialization.Serializable

@Serializable
data class TypedError(val error: String)