package me.yseeger.dgalumni.api.controller

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import me.yseeger.dgalumni.api.core.requireAdmin
import me.yseeger.dgalumni.api.data.Year
import me.yseeger.dgalumni.api.data.Years
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class CreateYearRequest(val year: Int, val isClass: Boolean = false)

fun Routing.years() {
    route("/years") {
        post {
            val account = requireAdmin() ?: return@post
            val req = Json.decodeFromString<CreateYearRequest>(call.receiveText())
            val yearExists = transaction {
                return@transaction Year.find { Years.year eq req.year }.any()
            }
            if (yearExists) {
                call.respond(HttpStatusCode.BadRequest, Json.encodeToString(TypedError("createYear/year-exists")))
                return@post
            }
            val year = transaction {
                return@transaction Year.new {
                    year = req.year
                    isClass = req.isClass
                }
            }
            call.respond(HttpStatusCode.Created)
        }
    }
}