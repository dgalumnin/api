plugins {
    application
    kotlin("jvm") version "1.4.0"
    kotlin("plugin.serialization") version "1.4.0"
}

group = "me.yseeger"
version = "1.0-SNAPSHOT"

repositories {
    jcenter()
}

dependencies {
    // Ktor
    implementation("io.ktor:ktor-server-core:1.4.0")
    implementation("io.ktor:ktor-server-netty:1.4.0")

    // Metrics
    implementation("io.ktor:ktor-metrics-micrometer:1.4.0")
    implementation("io.micrometer:micrometer-registry-prometheus:1.5.4")

    // Database
    implementation("org.jetbrains.exposed", "exposed-core", "0.25.1")
    implementation("org.jetbrains.exposed", "exposed-jdbc", "0.25.1")
    implementation("org.jetbrains.exposed", "exposed-dao", "0.25.1")
    implementation("org.jetbrains.exposed", "exposed-java-time", "0.25.1")
    implementation("com.zaxxer", "HikariCP", "3.4.5")
    implementation("org.postgresql", "postgresql", "42.2.15")

    // Util
    implementation("xyz.downgoon", "snowflake", "1.0.0")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:1.0.0-RC")
    implementation("at.favre.lib:bcrypt:0.9.0")

    // Logging
    implementation("org.slf4j:slf4j-api:2.0.0-alpha1")
    implementation("io.github.microutils:kotlin-logging:1.8.3")
    implementation("ch.qos.logback:logback-classic:1.3.0-alpha5")
    implementation("io.sentry:sentry-logback:1.7.30")
}

application {
    mainClassName = "me.yseeger.dgalumni.api.ApplicationKt"
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "13"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "13"
    }
}